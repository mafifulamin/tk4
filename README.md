# Tugas 1 PPW 2018 Gasal

## Anggota Kelompok

- Usman Sidiq
- Daffa Muhammad Rayhan
- Ariq Munif Kusuma
- Muhammad Afiful Amin

## Status

[![pipeline status](https://gitlab.com/basdat/badges/master/pipeline.svg)](https://gitlab.com/basdat/commits/master)
[![coverage report](https://gitlab.com/basdat/badges/master/coverage.svg)](https://gitlab.com/basdat/commits/master)

## Heroku App


## How to gitgud

### Quick Start

1. Clone repo dari gitlab dan masuk ke folder

   ```bash
   $ git clone 
   ```

2. Setelah masuk folder root `basdat`, buat virtual environment

   Windows:

   ```bash
   > python -m venv env
   ```

   Unix:

   ```bash
   $ virtualenv env --python=python3
   ```

3. Aktifkan virtual environment setiap saat ketika ingin mengedit project

   Windows:

   ```
   > env\Scripts\activate.bat
   ```

   Unix:

   ```bash
   $ source env/bin/activate
   ```

4. Ketika virtual environtment sudah aktif (ditandai dengan tanda `(env)` sebelum prompt shell,
   Install semua dependency project yang ada di requirements.txt

   ```bash
   (env) $ pip install -r requirements.txt
   ```

### Setiap ngoding

1. Pastikan kalian sudah melakukan `Quick Start` dan jangan lupa aktifkan `Virtual Environtment`
2. Buat branch baru dari master dengan cara `git checkout -b [nama branch]`. Dengan mengerjakan tugas kalian di branch masing-masing akan meminimalisir konflik yang terjadi kedepannya.
3. Buat app sesuai dengan tugas kalian dengan perintah `django-admin startapp [nama app]`.
4. Setelah itu coba dulu commit dan push ke branch yang tadi udah dibuat 5engan cara `git push origin [nama branch]`.
5. Kalau mau ubah-ubah codingan, pastikan udah balik ke branch masing-masing ya. Bisa ngecek branchnya ada apa aja dengan command `git branch`. Nanti bakal keluar daftar branch yang ada dan sekarang lagi branch mana.
6. Kalau mau pindah ke branch (yang udah ada), bisa pakai command `git checkout [nama branch]`.
7. Jika merasa sudah selesai, silakan merge request ke branch master. Cara merge request ke master dapat dilihat [di sini](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).
8. Disarankan selalu commit setiap kali kalian selesai melakukan sesuatu sebagai save point.
9. Sering-sering lah pull dari master dengan cara `git pull origin master`, selesaikan konflik jika ada konflik, lalu merge branch kalian dengan master.
10. Jangan pernah langsung push ke master agar tidak terjadi gangguan. Selalu push ke branch masing-masing terlebih dahulu lalu merge request ke master.

### Menjalankan server di localhost

1. Pastikan sudah mengaktifkan virtual environment dan menginstall dependency
2. Jalankan perintah `collecstatic` (Jika diperlukan) agar semua static file bisa di load
   ```bash
   (env) $ python manage.py collectstatic
   ```
3. Jalankan perintah `makemigrations` (Jika diperlukan) setiap membuat models baru
   ```bash
   (env) $ python manage.py makemigrations
   ```
4. Jalankan perintah `migrate` (Jika diperlukan) agar database dapat sinkron dengan models
   ```bash
   (env) $ python manage.py migrate
   ```
5. Jalankan perintah `runserver` untuk mulai menjalankan server
   ```bash
   (env) $ python manage.py runserver
   ```
6. Web server aka berjalan dan dapat diakses melalui port 8000
7. Untuk menghentikan local server, gunakan kombinasi tombol `CTRL+C`
8. Untuk keluar dari virtual environment, jalankan perintah `deactivate`
